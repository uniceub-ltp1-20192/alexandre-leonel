cebolas = 300 #número total de cebolas
cebolas_na_caixa = 120 #número de cebolas dentro de caixas
espaco_caixa = 5 #número de cebolas que cabem em cada caixa
caixas = 60 #número de caixas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa #número de cebolas fora das caixas
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa) #número de caixas vazias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa #número de caixas necessárias para guardar todas as cebolas
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos, %.0f caixas vazias" % caixas_vazias)
print ("Então, precisamos de %.0f caixas para empacotar todas as cebolas" % caixas_necessarias)
