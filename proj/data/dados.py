from entities.democracia import Democracia
from validator.validador import Validator
class Dados:

    def __init__(self):
        self._dados = dict()
        self._id = 0

    def geradorId(self):
        self._id = self._id + 1
        return self._id

    def buscarPorId(self,paramId):
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            return self._dados.get(int(paramId))

    def buscarPorAtributo(self,param):
        lista = dict()
        ultimoEncontrado = 0
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            for p in self._dados.values():
                if p.area == param:
                    lista[p.id] = p
                    ultimoEncontrado = p.id
            if (len(lista) == 1):
                return lista[ultimoEncontrado]
            else:
                if (len(lista) == 0):
                    print("Nenhum encontrado")
                    return None            
                return self.buscarPorIdComLista(lista)

    def buscarPorIdComLista(self,lista):
        print("Existem múltiplos registros com o filtro informado:")
        for x in lista.values():
            print(x)
        print("Informe um id:")
        return lista.get(int(Validator.verificarInteiro()))

    def inserir(self,entidade):
        entidade.id = self.geradorId()
        self._dados[entidade.id] = entidade

    def alterar(self,entidade):
        self._dados[entidade.id] = entidade

    def deletar(self,entidade):
        del self._dados[entidade.id]