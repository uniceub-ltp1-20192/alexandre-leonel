from entities.pais import Pais

class Democracia(Pais):
	def __init__(self, eleicoes = 0):
		super().__init__()
		self._eleicoes = eleicoes
		
	def __str__(self):
		return """Dados da Democracia:
		Id: {}
		Nome: {}
		Área: {} km2
		População: {}
		PIB: {}
		Forma de governo: {}
		Frequência de eleições: {} anos
		""".format(self.id,
			self.nome,
			self.area,
			self.populacao,
			self.pib,
			self.formaGoverno,
			self.eleicoes)