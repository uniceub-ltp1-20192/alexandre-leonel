import re

class Validator:

	@staticmethod
	def validar(expReg,msgInvalido, msgValido):
		teste = False
		while teste == False:
			valor = input("Informe um valor:")
			verificarEx = re.match(expReg,valor)
			if verificarEx == None:
				print(msgInvalido.format(expReg))
			else:
				print(msgValido.format(valor))
				return valor

	@staticmethod
	def validarValorInformado(valorAtual,msg):
		novoValor = input(msg)
		if novoValor != None and novoValor != "":
			return novoValor
		return valorAtual

	@staticmethod
	def verificarInteiro():
		teste = False
		while teste == False:
			valor = input("")
			v = re.match("\d+",valor)
			if v != None:
				teste = True
		return int(valor)
