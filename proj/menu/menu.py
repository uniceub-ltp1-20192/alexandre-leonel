from validator.validador import Validator
from data.dados import Dados
from entities.democracia import Democracia
class Menu:
	@staticmethod
	def mainMenu():
		print("""
				0 - Sair
				1 - Consultar
				2 - Inserir
				3 - Alterar
				4 - Deletar""")
		return Validator.validar("[0-4]", "valor inválido.\n", "ok")
	@staticmethod
	def queryMenu():
		print("""
                0 - Voltar
                1 - Consultar por identificador
                2 - Consultar por propriedade
                    """)
		return Validator.validar("[0-4]", "valor inválido.", "ok")

	@staticmethod
	def iniciarMenu():
		opMenu = ""
		d = Dados()
		print("menu iniciado")
		while opMenu != "0":
			opMenu = Menu.mainMenu()
			if opMenu == "1":
				print("consulta")
				opMenuC = ""
				while opMenuC != "0":
					opMenuC = Menu.queryMenu()
					if opMenuC == "1":
						retorno = Menu.menuBuscaPorId(d)
						if(retorno != None):
							print(retorno)
						else:
							print("Nao foi encontrado nenhum registro com esse identificador")
					elif opMenuC == "2":
						Menu.menuBuscarPorAtributo(d)
						if(retorno != None):
							print(retorno)
						else:
							print("Nao foi encontrado nenhum registro com esse atributo")
					elif opMenuC == "0":
						print("Retornando")
					else:
						print("Valor inválido. Tente novamente.\n")
						
			elif opMenu =="2":
				print("inserir")
				Menu.menuInserir(d)
				
			elif opMenu == "3":
				print("Alterar")
				opMenuA = ""
				while opMenuA != "0":
					opMenuA = Menu.queryMenu()
					if opMenuA == "1":
						retorno = Menu.menuBuscaPorId(d)
						if(retorno != None):
							retorno = Menu.menuAlterar(retorno,d)
						else:
							print("Nao foi encontrado nenhum registro com esse identificador")
					elif opMenuA == "2":
						retorno = Menu.menuBuscarPorAtributo(d)
						if(retorno != None):
							retorno = Menu.menuAlterar(retorno,d)
						else:
							print("Nao foi encontrado nenhum registro com esse atributo")
					elif opMenuA == "0":
						print("Retornando")
					else:
						print("Valor inválido. Tente novamente.\n")
			elif opMenu == "4":
				print("Deletar")
				opMenuD = ""
				while opMenuD != "0":
					opMenuD = Menu.queryMenu()
					if opMenuD == "1":
						retorno = Menu.menuBuscaPorId(d)
						if retorno != None:
							Menu.menuDeletar(retorno,d)
					elif opMenuD == "2":
						Menu.menuBuscarPorAtributo(d)
						retorno = Menu.menuBuscarPorAtributo(d)
						Menu.menuDeletar(retorno,d)
					elif opMenuD == "0":
						print("Retornando")
					else:
						print("Valor inválido. Tente novamente.\n")
			elif opMenu == "0":
				print("Encerrando")
			else:
				print("Valor inválido. Tente novamente.\n")
	
	@staticmethod
	def menuInserir(d):
		de = Democracia()
		de.nome = input("Nome do país: ")
		de.area = input("Área(km2): ")
		de.populacao = input("População: ")
		de.pib = input("PIB: ")
		de.formaGoverno = input("Forma de governo: ")
		de.eleicoes = input("Tempo entre eleições(em anos): ")
		d.inserir(de)

	def menuAlterar(retorno,d):
		retorno.nome = Validator.validarValorInformado(retorno.nome,"Nome do país: ")
		retorno.area = Validator.validarValorInformado(retorno.area,"Área(km2): ")
		retorno.populacao = Validator.validarValorInformado(retorno.populacao,"População: ")
		retorno.pib = Validator.validarValorInformado(retorno.pib,"PIB: ")
		retorno.formaGoverno = Validator.validarValorInformado(retorno.formaGoverno,"Forma de governo: ")
		retorno.eleicoes = Validator.validarValorInformado(retorno.eleicoes,"Tempo entre eleições(em anos): ")
		d.alterar(retorno)

	def menuDeletar(retorno,d):
		print(retorno)
		deletar = input(
            """Deseja deletar o país ?
            S - Sim
            N - Nao
            """)
		if(deletar == "S" or deletar == "s"):
			d.deletar(retorno)
			print("Registro deletado")

	@staticmethod
	def menuBuscaPorId(d :Dados):        
		retorno = d.buscarPorId(
			Validator.validar(r'\d+','',''))
		return retorno

	@staticmethod
	def menuBuscarPorAtributo(d :Dados):
		retorno = d.buscarPorAtributo(
			input("Informe uma área: "))
		print(retorno)
