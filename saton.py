import sys
def askData():
	try:
		g, w = input("Insira a nota(0-10) e o peso(1-5):").split()
		g, w = [float(g), int(w)]
		if g < 0 or g > 10:
			print("Nota tem que ser entre 0 e 10")
			return askData()
		if w < 1 or w > 5:
			print("Peso tem que ser entre 1 e 5")
			return askData()
		return g, w
	except KeyboardInterrupt:
		print()
		sys.exit()
	except:
		print("Erro na insersão de dados.", sys.exc_info()[0], "Tente novamente.")
		return askData()
def addMore():
	v = input("Há mais notas para serem colocadas?(s/n)")
	if v == "s" or v == "S":
		return True
	if v == "n" or v == "N":
		return False
	print("Responda sim ou não")
	return addMore()
def sumGrades(g):
	return g
def sumWeights(w):
	return w
def sumWG(g, w):
	return g * w
def calculateNA(sg, n):
	na = sg / n
	print("Média: ", na)
def calculateWA(swg, sw):
	wa = swg / sw
	print("Média ponderada: ", wa)

v = True
number = 0
sum_of_grades = 0
sum_of_weights = 0
sum_of_weighted_grades = 0
while v:
	grade, weight = askData()
	number += 1
	sum_of_grades += sumGrades(grade)
	sum_of_weights += sumWeights(weight)
	sum_of_weighted_grades += sumWG(grade, weight)
	v = addMore()
calculateNA(sum_of_grades, number)
calculateWA(sum_of_weighted_grades, sum_of_weights)
