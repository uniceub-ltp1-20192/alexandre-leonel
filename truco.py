import sys
from random import shuffle

SUITS          = ("SPADES", "HEARTS", "DIAMONDS", "CLUBS")
CARD_MAX_VALUE = 13
CARD_MIN_VALUE = 1

def generateCardDeck():
	
	cards = []

	for suit in SUITS:
		for value in range(CARD_MIN_VALUE, CARD_MAX_VALUE + 1):
			card = (suit, value)
			cards.append(card)
		cards.append('JOKER1')
		cards.append('JOKER2')

	return cards

def deckTruco():
	cards = []
	for suit in SUITS:
		for value in range(CARD_MIN_VALUE, CARD_MIN_VALUE + 3):
			card = (suit, value)
			cards.append(card)
		for value in range(CARD_MAX_VALUE - 2, CARD_MAX_VALUE + 1):
			card = (suit, value)
			cards.append(card)
	cards.append(('HEARTS', 7))
	cards.append(('DIAMONDS', 7))
	cards.append(('CLUBS', 4))
	cards.append(('JOKER', 0))
	cards.append(('JOKER', 0))
	return cards

def assignValues(cards):
	valued_cards = []
	for i in range(len(cards)):
		valued_card = []
		valued_card.append(cards[i])
		if cards[i][1] == 11:
			valued_card.append(1)
			valued_card.append('Valete ')
		if cards[i][1] == 12:
			valued_card.append(2)
			valued_card.append('Rainha ')
		if cards[i][1] == 13:
			valued_card.append(3)
			valued_card.append('Rei ')
		if cards[i][1] == 1 and cards[i][0] != 'SPADES':
			valued_card.append(4)
			valued_card.append('Ás ')
		if cards[i][1] == 2:
			valued_card.append(5)
			valued_card.append('Dois ')
		if cards[i][1] == 3:
			valued_card.append(6)
			valued_card.append('Tres ')
		if cards[i][0] == 'JOKER':
			valued_card.append(7)
			valued_card.append('Coringa')
			valued_card.append('')
		if cards[i][1] == 7 and cards[i][0] == 'DIAMONDS':
			valued_card.append(8)
			valued_card.append('Sete ')
		if cards[i][1] == 1 and cards[i][0] == 'SPADES':
			valued_card.append(9)
			valued_card.append('Ás ')
		if cards[i][1] == 7 and cards[i][0] == 'HEARTS':
			valued_card.append(10)
			valued_card.append('Sete ')
		if cards[i][1] == 4:
			valued_card.append(11)
			valued_card.append('Quatro ')
		if cards[i][0] == 'SPADES':
			valued_card.append('de espadas')
		if cards[i][0] == 'HEARTS':
			valued_card.append('de copas')
		if cards[i][0] == 'DIAMONDS':
			valued_card.append('de ouros')
		if cards[i][0] == 'CLUBS':
			valued_card.append('de paus')
		valued_card[2 : 4] = [''.join(valued_card[2 : 4])]
		valued_cards.append(valued_card)
	return valued_cards

def generateShuffleDeck(cards):
		deck = cards
		shuffle(deck)
		return deck

def distributeCards(deck, number_of_players, number_of_cards):
	table = []
	for i in range(number_of_players):
		hand = []
		for j in range(number_of_cards):
			hand.append(deck.pop())
		table.append(hand)
	return table, deck
	
def selectCard(player, table):
	for i in range(len(player)):
		print(player[i][2])
	try:
		select_card = int(input("Qual carta você vai jogar(começa pelo 1)?"))
		if select_card < 1:
			print("Valor inválido. Tente novamente.")
			return selectCard(player, table)
		table.append(player.pop(select_card - 1))
	except KeyboardInterrupt:
		print()
		sys.exit()
	except:
		print("Valor inválido. Tente novamente.")
		return selectCard(player, table)

def checkValues(table, i):
	if table[0][1] > table[1][1] and table[0][1] > table[3][1] or table[2][1] > table[1][1] and table[2][1] > table[3][1]:
		return 1
	if table[1][1] > table[0][1] and table[1][1] > table[2][1] or table[3][1] > table[0][1] and table[3][1] > table[2][1]:
		return 2
	return 3

def turn(player1, player2, player3, player4, table, discard):
	order = []
	victory1 = 0
	victory2 = 0
	order.append(player1)
	order.append(player2)
	order.append(player3)
	order.append(player4)
	while order[0] != []:
		for i in range(len(order)):
			selectCard(order[i], table)
		a = i % 2
		value = checkValues(table, a)
		if value == 1 : victory1 += 1
		if value == 2 : victory2 += 1
		if victory1 == 2: return 1
		if victory2 == 2: return 2
		for i in range(len(order)):
			discard.append(table.pop())
	return 3

def reshuffle(p1, p2, p3, p4, t, d, deck):
	deck = deck + p1 + p2 + p3 + p4 + t + d
	p1.clear()
	p2.clear()
	p3.clear()
	p4.clear()
	t.clear()
	d.clear()
	shuffle(deck)
	return deck
		

discard_pile = []
point1 = 0
point2 = 0
deck = generateShuffleDeck(deckTruco())
deck = assignValues(deck)
while True:
	table, new_deck = distributeCards(deck, 4, 3)
	player1 = table.pop()
	player2 = table.pop()
	player3 = table.pop()
	player4 = table.pop()
	winner = turn(player1, player2, player3, player4, table, discard_pile)
	if winner == 1:
		point1 += 1
		print("Dupla 1 vence essa rodada.")
	elif winner == 2:
		point2 += 1
		print("Dupla 2 vence essa rodada.")
	else : print("Empate.")
	print("Dupla1", point1, "x", point2, "Dupla2")
	if point1 == 12:
		print("Dupla 1 ganha!")
		break
	if point2 == 12:
		print("Dupla 2 ganha!")
		break
	deck = reshuffle(player1, player2, player3, player4, table, discard_pile, deck)
